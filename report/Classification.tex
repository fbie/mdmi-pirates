\section{Classification}

The aim of this section is to introduce the pre-processing steps,
methods and results from running known classification algorithms to
try estimating which ships to rob and which to let pass unnoticed.

In other words we will be classifying shipments into two classes,
\textit{profitable} and \textit{not profitable}. A ship will be deemed
profitable if it carries cargo exceeding a size of $100 m^3$. While this is
generally a bad choice for evaluating the value of a cargo, the codes for goods
are shipper-dependent. This means that we have no way of creating a mapping
between cargo codes and value. We will assume that information about the
shipments origin, destination, shipper and consignee will be visible to an
experienced pirate with a good binocular.

\subsection{Building the Model}
The classification model works by inferring a class $C$ from a given vector $x$
as shown in Fig.~\ref{fig:classification-model}.

\begin{figure}

  \centering
  \makebox[\textwidth][c]{
    \begin{tabular}{|llllll|l|}
      \hline
      $x_1$ & $x_2$ & $x_3$ & $x_4$ & $x_5$ & $x_6$ & Class \\
      \hline
      Shipper & Consignee & Origin(Port) & Destination(Port) & Arrival(Date) & Departure(Date) & Profitable? \\ 
      Nominal & Nominal   & Ratio        & Ratio             & Interval      &
      Interval        & Ordinal     \\
      \hline
    \end{tabular}
  } % makebox

  \caption{Classification Model}
  \label{fig:classification-model}
\end{figure}

\subsection{KNN}

The first algorithm is also the simplest. KNN attempts to classify entries by
comparing them to a list of entries of known class. To do so we first define a
function that can be used to determine the distance between two entries.

\subsubsection{Distance Function}
The model includes variables on all different levels of measurement and it is
necessary to be very careful when specifying the distance between two
shipments. For the nominal variables we simply look at whether they are
different and give a distance between different values and no distance
otherwise. The variables concerned with ports have a distance which is defined
as the actual physical distance between the two ports. This is calculated from
the GPS coordinates as given by the UN location codes (see
Sec.~\ref{sec:data}). The distance between two dates is given by the seconds
passed between the two dates. Our distance function $D$ is therefore denoted as

\begin{equation}
  D(x,y) = \sum_{i=1}^2 w_i \cdot isequal(x_i, y_i) + \sum_{i=3}^6w_i \cdot distance(x_i,y_i)
\end{equation}

where $w_i = 100$ for $i \in \{1, 2\}$ and $w_i = 1$ for $i \in \{3,
6\}$. These weights where picked to give each aspect of the distance
function an about equal impact. An expert with better domain knowledge
would be able to adjust these weights to improve the distance
function, or we could improve them by experimenting, at the risk of
over fitting. The temporal aspect of the data is tricky to deal
with. Any newly observed shipment will be closer to the last shipments
in the data set, simply because nothing new can ever occur in the
past. We therefore neglect the time attributes of a shipment for $D'$:

\begin{equation}
  D'(x,y) = \sum_{i=1}^2 w_i \cdot isequal(x_i, y_i) + \sum_{i=3}^4w_i \cdot distance(x_i,y_i)
\end{equation}

This means, that the time attributes of a shipment are not considered
in KNN classification.

\subsubsection{Results}
\label{subsubsec:knn-classification}

\begin{figure}
  \centering

  \makebox[\textwidth][c]{
    \begin{tabular}{l|ll||l|l}
                  & Predicted Raid & Predicted Pass & Total   & Recognition \\ \hline
      Actual Raid & $16320$ & $15819$  & $32139$  & $50,78\%$ \\
      Actual Pass & $10267$ & $110092$ & $120359$ & $91,47\%$ \\ \hline
      Total       & $26587$ & $125911$ & $152498$ & $82,89\%$ \\
    \end{tabular}
  } % makebox

  \caption{Extended confusion matrix for KNN classifying 152498 samples for $k=5$.}
  \label{fig:confusion-matrix-knn}
  
\end{figure}

The ~300.000 data-entries were split into 150.000 training entries and 150.000
test entries. Incomplete entries were in the order $<2\%$ so they were simply
excluded. We split the data at random, taking every second row. Our KNN
implementation has a run-time of $O(n^2 \cdot log(n))$, $n$ being the size of
the data set to classify. On a modern PC we where able to classify the data in
around $220$ minutes. The results can be read from the confusion matrix in
Fig.~\ref{fig:confusion-matrix-knn}.

The accuracy of KNN is calculated according to Eq.~\ref{eq:accuracy-KNN}. Since
the data set represents a majority of the negative class, there is a high
baseline accuracy of $78,9\%$ (see Subsec.~\ref{subsec:descriptive-stats}). The
sensitivity and specificity give a more precise picture of the models
capabilities.

\begin{equation}
  accuracy=\frac{TP + TN}{P+N}=\frac{16320+110092}{32139+120359}=82,89\%
  \label{eq:accuracy-KNN}
\end{equation}

This result is not a great improvement over the baseline
accuracy. Looking at the sensitivity KNN only identifies $50\%$ of the
ships where a raid is profitable and let's the other half slip by. It
does do a decent job on the negative cases, recommending less than
$10\%$ of unprofitable ships to be raided. In this way the KNN
algorithm gives a conservative estimate of when to raid . 

\subsubsection{Suggested Improvements}
KNN had limited success despite being given a large set of data to
train on.  A possible problem could be in the binary representation of
consignees and shippers. They may have more in common than a simple
equality test of their names can reveal. Improvements could also be
made in the adjustment of the weights and decisions made in the
definition of the distance function in general. 

\input{ID3}
