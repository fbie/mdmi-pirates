package dk.itu.shipments;
import java.util.Arrays;

public class ItemSet {
	
	private static final int[] PRIMES = { 2, 3, 5, 7, 11, 13, 17, 23, 27, 31, 37 };
    final int[] set;

    public ItemSet( int[] set ) {
        this.set = set;
    }

    @Override
    public int hashCode() {
        int code = 0;
        for (int i = 0; i < set.length; i++) {
            code += set[i] * PRIMES[i];
        }
        return code;
    }
    /*
     * the order of elements in the dataset should not matter
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals( Object o ) {
        if (!(o instanceof ItemSet)) {
            return false;
        }
        ItemSet other = (ItemSet) o;
        if (other.set.length != this.set.length) {
            return false;
        }
        // arrays have to be sorted before comparing
        Arrays.sort(set);
    	Arrays.sort(other.set);
        
        for (int i = 0; i < set.length; i++) {
            if (set[i] != other.set[i]) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public String toString() {
    	String output = "";
    	
    	for (int item : set) {
    		output += item + ",";
    	}
    	
    	return output;
    }
}
