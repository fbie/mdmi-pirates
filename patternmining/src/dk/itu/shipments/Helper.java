package dk.itu.shipments;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Arrays;

import dk.itu.shipments.utils.Log;

public class Helper {
	public static String[][] resultSet2Array(ResultSet result, int size) throws ParseException {
		String[][] shippers = new String[size][];
		int i = 0;
		if (result != null) {
			
			try {
				while (result.next()) {
					String items = result.getString("products");
					String[] transactionItems = items.split(",");
					
					shippers[i] = transactionItems;
					i++;
				}
			} catch (SQLException e) {
				Log.error(e.toString());
			}
		}
		return shippers;
	}
}
