package dk.itu.shipments;
import java.security.AllPermission;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Set;

import javax.swing.text.html.MinimalHTMLWriter;


public class AprioriAlg {
    static int[][] TRANSACTIONS;
    // keeps all the frequent items
    static Hashtable<ItemSet, Integer> allFrequentItemSets = new Hashtable<ItemSet, Integer>();

    /*public static List<ItemSet> apriori(int supportThreshold, int minimum_support ) {
        int k;
        
        Hashtable<ItemSet, Integer> frequentItemSets = generateFrequentItemSetsLevel1(minimum_support );
        
        List<ItemSet> frequentItemsList= new ArrayList<ItemSet>();
    
        for (k = 1; frequentItemSets.size() > 0; k++) {
            frequentItemSets = generateFrequentItemSets( minimum_support, frequentItemSets );
            
            
            if (frequentItemSets.isEmpty() == false) {
            	System.out.print( "Frequent itemsets of length " + (k + 1) + frequentItemSets.size() + "found :");
	            Enumeration e = frequentItemSets.keys();
		          //iterate through Hashtable keys Enumeration
		  	      while(e.hasMoreElements()) {
		  	    	  ItemSet s =(ItemSet)e.nextElement(); 
		  	    	  // add ItemSet to frequent items list
		  	    	  frequentItemsList.add(s);
		  	    	  
		  	    	  System.out.println(s + " - " + frequentItemSets.get(s));
		  	    	  
		  	    	  create_association(s, supportThreshold);
		  	      }
            }
        }
        
        return null;
    }*/
    
    public static void apriori(int supportThreshold, int minimum_support, List<String> realValues ) {
        int k;
        
        
        Hashtable<ItemSet, Integer> frequentItemSets = generateFrequentItemSetsLevel1(minimum_support );
        List<ItemSet> frequentItemsList= new ArrayList<ItemSet>();
    
        for (k = 1; frequentItemSets.size() > 0; k++) {
            frequentItemSets = generateFrequentItemSets( minimum_support, frequentItemSets );
            
            if (frequentItemSets.isEmpty() == false) {
            	
            	System.out.println("\n Frequent itemsets of length " + (k + 1) + " \t " + frequentItemSets.size() + " found : \n");
	            Enumeration e = frequentItemSets.keys();
		          //iterate through Hashtable keys Enumeration
		  	      while(e.hasMoreElements()) {
		  	    	  ItemSet s =(ItemSet)e.nextElement(); 
		  	    	  // add ItemSet to frequent items list
		  	    	  frequentItemsList.add(s);
		  	    	  // itemset to frequent items hashtable
		  	    	  
		  	    	  //System.out.println(item2Lang(realValues, s) + " - " + allFrequentItemSets.get(s) + " appearances");
		  	    	  System.out.println(item2Lang(realValues, s) + " - " + countSupport(s.set) + " appearances");
		  	    
		  	    	  create_association_lang(s, supportThreshold, realValues);
		  	      }
            }
        }
        
    }
    
    private static String item2Lang(List<String> realValues, ItemSet s) {
    	String listLangs = "[";
    	for (int i : s.set) {
    		listLangs += realValues.get(i) + ",";
    	}
    	listLangs += "]";
    	return listLangs;
    }
    
    private static void create_association_lang(ItemSet item, int supportThreshold, List<String> realValues) {
    	for (int i = 0; i < item.set.length; i++) {
    		// generate all the possible n-1 subsets and find them in the hash table
    		int[] firstItem = new int[1];
    		firstItem[0] = item.set[i];
    		ItemSet first = new ItemSet(firstItem);
    		ItemSet second;
    		List<Integer> subsetItems =  new ArrayList<Integer>();
    		for(int j = 0; j < item.set.length; j++) {
    			if (j != i)
    				subsetItems.add(item.set[j]);
    		}
    		int[] newItemsArray = toIntArray(subsetItems);
    		second = new ItemSet(newItemsArray);
    		
    		double confidence = 100 * ((double)countSupport(item.set)/(double)countSupport(firstItem));
    		if (confidence >= supportThreshold)
    			System.out.println("Association rules \n" + realValues.get(item.set[i]) + " => " + item2Lang(realValues, second) + " confidence " + confidence + "%");
    	}
    }
    
    private static void create_association(ItemSet item, int supportThreshold) {
    	for (int i = 0; i < item.set.length; i++) {
    		// generate all the possible n-1 subsets and find them in the hash table
    		int[] firstItem = new int[1];
    		firstItem[0] = item.set[i];
    		ItemSet first = new ItemSet(firstItem);
    		ItemSet second;
    		List<Integer> subsetItems =  new ArrayList<Integer>();
    		for(int j = 0; j < item.set.length; j++) {
    			if (j != i)
    				subsetItems.add(item.set[j]);
    		}
    		int[] newItemsArray = toIntArray(subsetItems);
    		second = new ItemSet(newItemsArray);
    		
    		double confidence = 100 * ((double)countSupport(item.set)/(double)countSupport(firstItem));
    		if (confidence >= supportThreshold)
    			System.out.println(item.set[i] + " => " + second.toString() + " confidence " + confidence);
    	}
    }
    
    private static Hashtable<ItemSet, Integer> generateFrequentItemSets( int supportThreshold, 
                    Hashtable<ItemSet, Integer> lowerLevelItemSets ) {
        // TODO: first generate candidate itemsets from the lower level itemsets
    	
    	Hashtable<ItemSet, Integer> newFrequentItemSets = new Hashtable<ItemSet, Integer>();
    	// get the keys of array to generate new items
    	Set<ItemSet> keys = lowerLevelItemSets.keySet();
    	ItemSet[] k = new ItemSet[keys.size()];
    	int n = 0;
    	for(ItemSet key: keys){
            k[n] = key;
            n++;
        }
    	
    	for (int i=0; i<k.length-1; i++) {
    		for (int j=i+1; j<k.length; j++) {
    			
    			ItemSet join = joinSets(k[i], k[j]);
	    		  
	    		  if (join != null && has_infrequent_subset(join, lowerLevelItemSets, supportThreshold) && !alreadyPresent(newFrequentItemSets, join)) {
	    			  int support = countSupport(join.set);
	    			  if (support >= supportThreshold) { 
	    				  newFrequentItemSets.put(join, support);
	    				  allFrequentItemSets.put(join, support);
	    			  }
	    		  }
    		}
    	} 
    	
        return newFrequentItemSets;
    }
    
    private static boolean has_infrequent_subset(ItemSet newItem, Hashtable<ItemSet, Integer> ItemSets, int min_sup) {
    	for (int i = 0; i < newItem.set.length; i++) {
    		// generate all the possible n-1 subsets and find them in the hash table
    		ItemSet subset;
    		List<Integer> subsetItems =  new ArrayList<Integer>();
    		for(int j = 0; j < newItem.set.length; j++) {
    			if (j != i)
    				subsetItems.add(newItem.set[j]);
    		}
    		int[] newItemsArray = toIntArray(subsetItems);
    		subset = new ItemSet(newItemsArray);
    		// tries to find the subset in the list of transactions
    		if (countSupport(subset.set) < min_sup)
    			return false;
    	}
    	
    	return true;
    }
    
    /*
     * checks if the itemset is already present in the frequent items list
     */
    private static boolean alreadyPresent(Hashtable<ItemSet, Integer> newFrequentItemSets, ItemSet newItemset) {
    	Set<ItemSet> keys = newFrequentItemSets.keySet();

    	for(ItemSet key : keys){
           if (key.equals(newItemset))
        	   return true;
        }
    	
    	return false;
    }
    
    
    private static ItemSet joinSets( ItemSet first, ItemSet second ) {
        // TODO: return something useful
    	List<Integer> newItems =  new ArrayList<Integer>();
    	int k = first.set.length + 1;
    	// they should have some items in common
    	int itemsToMatch = k-2;
    	Arrays.sort(first.set);
    	Arrays.sort(second.set);
    	for(int i = 0 ; i < first.set.length; i++) {
    		if (Arrays.binarySearch(second.set, first.set[i]) >= 0) {
    			itemsToMatch -=1;
    		}
    		
    		newItems.add(first.set[i]);
    	}
    	
    	for(int i = 0 ; i < second.set.length; i++) {
    		if (Arrays.binarySearch(first.set, second.set[i]) < 0) {
    			newItems.add(second.set[i]);
    		}
    	}
    	
    	// have enough items in common
    	if (itemsToMatch == 0) {
	    	// convert arraylist to array
	    	int[] newItemsArray = toIntArray(newItems);
	    	ItemSet newItemSet = new ItemSet(newItemsArray);
	    	return newItemSet;
	    }
    	else 
    		return null;
    }
    
    private static Hashtable<ItemSet, Integer> generateFrequentItemSetsLevel1(int supportThreshold ) {
    	 Hashtable<ItemSet, Integer> frequentItemSetsLevel1= new Hashtable<ItemSet, Integer>();
    	 
    	int[] items;
    	// TODO: return something useful
        for (int[] transaction : TRANSACTIONS) {
        	for(int value : transaction) {
        		int[] valueArray = new int[1];
        		valueArray[0] = value;
        		ItemSet set = new ItemSet(valueArray);
        	
        		if (!frequentItemSetsLevel1.containsKey(set)) {
        			int support = countSupport(valueArray);
        			if (support >= supportThreshold)
        				frequentItemSetsLevel1.put(set, support);
        				allFrequentItemSets.put(set, support);
        		}
        	}	
        }
    	
    	return frequentItemSetsLevel1;
    }

    public static int countSupport( int[] itemSet) {
        // Assumes that items in ItemSets and transactions are both unique
    	int counter =  0;
    	
         for (int[] arrays : TRANSACTIONS) {
        	 boolean found = true;
        	 Arrays.sort(arrays); 
         	 for(int key : itemSet) {
         		if(Arrays.binarySearch(arrays, key) < 0) {
         			found = false;
         			break;
         		}
         	 }
         	 if (found != false)
         		 counter++;
         }
    	
        return counter;
    }
  
    // transforms a list to array
    public static int[] toIntArray(List<Integer> list) {  
    	int[] intArray = new int[list.size()];  
    	int i = 0;  
    	   
    	for (Integer integer : list)  
    		intArray[i++] = integer;  
    	  
    	return intArray;  
    } 
}
