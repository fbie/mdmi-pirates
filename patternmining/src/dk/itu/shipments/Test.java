package dk.itu.shipments;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import dk.itu.shipments.utils.*;

public class Test {

	/**
	 * @param args
	 * @throws SQLException 
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws SQLException, ParseException {
		// TODO Auto-generated method stub
		/* Start applying apriori */
		final int supportThreshold = 70;
        final int min_sup = 5;
		
        Backend.connect();
        
        ResultSet result = Backend.select("* from pattern_mining_daily_products");
        ResultSet countResult = Backend.select("count(*) as total from pattern_mining_daily_products");
        
        int size = 0;
        // get the size of the resultSet
        while (countResult.next()) {
        	size = countResult.getInt("total");
        }
        
        try {
			String[][] products =  Helper.resultSet2Array(result, size);
			// transform programming languages values to int values to use in apriori 
			List<String> productCodes = new ArrayList<String>();
			
			for (String[] transaction : products) {
				for (int j = 0; j < transaction.length; j++) {
					if (!productCodes.contains(transaction[j].toLowerCase())) {
						productCodes.add(transaction[j].toLowerCase());
					}
					
				}
			}
			
			for (String s : productCodes) 
				System.out.println(s);
			
			// get transactions out of the programming languages options
	        int[][] transactions = new int [size][4]; 
	        for (int p=0; p < size; p++) {
	        	System.out.println();
	        	transactions[p] = new int [products[p].length];
	        	for (int q = 0; q < products[p].length; q++) {
	        		transactions[p][q] = productCodes.indexOf(products[p][q].toLowerCase());
	        		System.out.print("[" + p +  "][" + q +  "] = " + transactions[p][q]);
	        	}
	        }
	        
	        AprioriAlg.TRANSACTIONS = transactions;
	        
	        AprioriAlg.apriori(supportThreshold, min_sup, productCodes);
	        
	        
			
			//System.out.println(productCodes.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		
        
        
        
        /*
         *
		http://stackoverflow.com/questions/149772/how-to-use-group-by-to-concatenate-strings-in-mysql
        
         */
        
        
        
        // transform programming languages values to int values to use in apriori 
     	/*List<String> languages = new ArrayList<String>();
        
        AprioriAlg.TRANSACTIONS = new int[][] {
        		{0,1,2,3}, {0,1,2,3}, {1,2,3},
        		{0,1,2,3}, {0,1,2,3}, {1,2,3},
        		{1,2,3}, {1,2,3}, {1,2,0}
        };

        languages.add("0");
        languages.add("1");
        languages.add("2");
        languages.add("3");
        
        // call the apriori method
        AprioriAlg.apriori(supportThreshold, min_sup, languages);*/
	}

}
