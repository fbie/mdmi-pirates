/**
 * This file is part of dk.itu.shipments.
 *
 * dk.itu.shipments is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.itu.shipments.utils.Backend;

public class ShipmentTest {

	static int QUERY_LIMIT = 30;

	@Before
	public void setUp() throws Exception {
		Backend.connect();
	}

	@After
	public void tearDown() throws Exception {
		Backend.disconnect();
	}

	@Test
	public void testCreateShipmentsFromResultSet() {
		try {
			ResultSet result = Backend.select("* from ShipmentOrder limit " + QUERY_LIMIT);

			ArrayList<Shipment> shipments = Shipment.createShipmentsFromResultSet(result);
			assertEquals(QUERY_LIMIT, shipments.size());

			result.close();
		} catch (SQLException e) {
			fail(e.toString());
		}
	}

}
