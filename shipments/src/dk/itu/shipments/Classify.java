/**
 * This file is part of dk.itu.shipments.
 *
 * dk.itu.shipments is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import dk.itu.classifiers.KNN;
import dk.itu.classifiers.interfaces.IClassifier;
import dk.itu.shipments.utils.Backend;
import dk.itu.shipments.utils.Log;

public class Classify {

	public static class Timer {

		long startTime;

		private Timer(long time) {
			startTime = time;
		}

		/**
		 * (Re)Start the timer.
		 */
		public static Timer start() {
			return new Timer(Calendar.getInstance().getTimeInMillis());
		}

		/**
		 * @return Time in MS since start.
		 */
		public long getTime() {
			return Calendar.getInstance().getTimeInMillis() - startTime;
		}
	}

	// flags that indicate which classifiers to run
	static boolean doID3 = false;
	static boolean doKNN = false;

	// the k variable for KNN classification
	static int k = 5;

	static int limit = -1;
	static int repetition = 1;

	static boolean printTree = false;

	/**
	 * @return An array list containing all shipment data that was fetched from backend.
	 */
	public static ArrayList<Shipment> getData() {
		Log.debug(5, "Fetching data from backend and generating samples");
		ResultSet dataSet;
		if (limit < 0)
			dataSet = Backend.selectNoThrow("* from ShipmentOrder");
		else
			dataSet = Backend.selectNoThrow("* from ShipmentOrder limit " + limit);

		Log.debug(6, "Generating samples");
		Timer timer = Timer.start();
		ArrayList<Shipment> data = Shipment.createShipmentsFromResultSet(dataSet);
		Log.debug(2, "Took " + timer.getTime() + "ms");

		try {
			dataSet.close();
		} catch (SQLException e) {
			Log.error(e.toString());
		}

		Log.debug(5, "Done generating " + data.size() + " samples");
		return data;
	}

	/**
	 * Partition a given data set
	 * @param data The initial data set
	 * @param trainingSet Empty list where all the training data will be written into.
	 * @param testSet Empty list where all the test data will be written into.
	 */
	public static void partition(final ArrayList<Shipment> data, final ArrayList<Shipment> testSet, final ArrayList<Shipment> trainingSet) {
		Log.debug(5, "Partitioning " + data.size() + " shipments into test set and training set");
		if (!trainingSet.isEmpty() || ! testSet.isEmpty())
			Log.error("trainingSet and testSet are not empty!");

		ArrayList<Shipment> shipments = new ArrayList<Shipment>(data);
		Collections.shuffle(shipments);

		testSet.addAll(shipments.subList(0, shipments.size() / 2));
		trainingSet.addAll(shipments.subList(shipments.size() / 2, shipments.size()));
	}

	/**
	 * Classifies data using given classifier.
	 * @param classifier Used for classification
	 * @param testSet Data to classify
	 * @param trainingSet Data to train on
	 */
	public static void classify(final IClassifier<Shipment, ShipmentLabel> classifier,
			final ArrayList<Shipment> testSet,
			final ArrayList<Shipment> trainingSet,
			final String type) {

		int positive = 0;
		int negative = 0;
		int truePositive = 0;
		int trueNegative = 0;
		int falsePositive = 0;
		int falseNegative = 0;
		int failPositive = 0;
		int failNegative = 0;

		Log.debug(0, "Start classifying");
		Timer timer = Timer.start();

		for (Shipment sample : testSet) {
			ShipmentLabel label = classifier.classify(sample);

			if (sample.classLabel() == ShipmentLabel.Profitable) {
				positive++;
				if (label == ShipmentLabel.Profitable)
					truePositive++;
				else if (label == ShipmentLabel.NotProfitable)
					falseNegative++;
				else
					failPositive++;
			} else if(sample.classLabel() == ShipmentLabel.NotProfitable) {
				negative++;
				if (label == ShipmentLabel.Profitable)
					falsePositive++;
				else if (label == ShipmentLabel.NotProfitable)
					trueNegative++;
				else
					failNegative++;
			}
		}

		Log.debug(2, "Took " + timer.getTime() + "ms");
		CSVReporter.report(type, positive, negative, truePositive, trueNegative, falsePositive, falseNegative, failPositive, failNegative);
	}

	/**
	 * Classifies data using KNN.
	 * @param testSet Data to classify
	 * @param trainingSet Data to train on
	 */
	public static void classifyKNN(final ArrayList<Shipment> testSet, final ArrayList<Shipment> trainingSet) {
		Log.debug(0, "Running KNN classifier with k = " + k + ".");
		Log.debug(0, "Training on " + trainingSet.size() + " samples");

		Timer timer = Timer.start();
		KNN<Shipment, ShipmentLabel> classifier = new KNN<Shipment, ShipmentLabel>(k, trainingSet);
		Log.debug(2, "Took " + timer.getTime() + "ms");

		classify(classifier, testSet, trainingSet, "KNN,k = " + k);
	}

	/**
	 * Classifies data using ID3.
	 * @param testSet Data to classify
	 * @param trainingSet Data to train on
	 */
	public static void classifyID3(final ArrayList<Shipment> testSet, final ArrayList<Shipment> trainingSet) {
		Log.debug(0, "Running ID3 classifier.");
		Log.debug(0, "Training on " + trainingSet.size() + " samples");

		Timer timer = Timer.start();
		ID3ShipmentClassifier classifier = new ID3ShipmentClassifier(trainingSet);
		Log.debug(2, "Took " + timer.getTime() + "ms");

		if (printTree)
			System.out.println(classifier.toString());
		else
			classify(classifier, testSet, trainingSet, "ID3");
	}

	/**
	 * Prints out help message.
	 */
	public static void printHelp() {
		String msg = "Options:\n" +
				"  -h, --help:		Print this help text and exit.\n" +
				"  -k <value>		K value for the KNN classifier.\n" +
				"  --knn			Run KNN classifier.\n" +
				"  --id3			Run ID3 classifier.\n" +
				"  -l, --limit <value>	Set the limit of data to fetch.\n" +
				"  -d, --debug <value>	Set debug output level.\n" +
				"  -r, --repeat <value> Number of repititions to run.\n" +
				"  -t, --tree       Instead of CSV report, print ID3 tree. Stops before classifying.";
		System.out.println(msg);
	}

	/**
	 * Parses program arguments and stores settings globally
	 * @param args Arguments to parse.
	 */
	public static void parseArgs(final String[] args) {
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			if (arg.equals("--help") || arg.equals("-h")) {
				printHelp();
				System.exit(0);
			} else if (arg.equals("--knn")) {
				doKNN = true;
			} else if (arg.equals("--id3")) {
				doID3 = true;
			} else if (arg.equals("-k")) {
				k = Integer.valueOf(args[++i]);
				if (k <= 0) {
					Log.error("k must be greater than zero (is " + k + ")");
					System.exit(1);
				}
			} else if (arg.equals("-l") || args.equals("--limit")) {
				limit = Integer.valueOf(args[++i]);
			} else if (arg.equals("-d") || arg.equals("--debug")) {
				Log.setLevel(Integer.valueOf(args[++i]));
			} else if (arg.equals("-r") || arg.equals("--repeat")) {
				repetition = Integer.valueOf(args[++i]);
			} else if (arg.equals("-t") || arg.equals("--tree")) {
				printTree = true;
			}
		}

		// printTree only makes sense in this configuration
		if (printTree && ! doID3) {
			Log.debug(3, "printTree is true, setting doKNN to false, doID3 to true, repetition to 1");
			repetition = 1;
			doKNN = false;
			doID3 = true;
		}

		Log.debug(6, "doKNN: " + (doKNN? "true" : "false")
				+ ", doID3: " + (doID3? "true" : "false")
				+ ", k = " + k
				+ ", limit = " + limit
				+ ", repetition = " + repetition);
	}

	/**
	 * @param args See printHelp()
	 */
	public static void main(final String[] args) {
		parseArgs(args);

		Backend.connect();
		ArrayList<Shipment> data = getData();

		for (int i = 0; i < repetition; i++) {
			Log.debug(0, "Running test #" + (i + 1));

			ArrayList<Shipment> testSet = new ArrayList<Shipment>();
			ArrayList<Shipment> trainingSet = new ArrayList<Shipment>();
			partition(data, testSet, trainingSet);

			if (doKNN || doID3) {
				if (doKNN)
					classifyKNN(testSet, trainingSet);
				if (doID3)
					classifyID3(testSet, trainingSet);
			} else {
				System.out.println("Neither KNN nor ID3 has been selected to run, no classifiers have been executed!");
			}
		}

		if (!printTree)
			CSVReporter.print();
		Backend.disconnect();
	}

}
