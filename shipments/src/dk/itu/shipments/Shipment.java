/**
 * This file is part of dk.itu.shipments.
 *
 * dk.itu.shipments is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


import dk.itu.common.interfaces.ISampleVector;
import dk.itu.common.interfaces.ISplittable;
import dk.itu.shipments.utils.Log;
import dk.itu.shipments.utils.PortUtils;
import dk.itu.shipments.utils.coords.Vec3;

public class Shipment implements ISampleVector<Shipment, ShipmentLabel>, ISplittable<Shipment, ShipmentLabel, ShipmentAttribute> {

	// parses dates from strings into proper objects
	static SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");

	public final String shipper;
	public final String consignee;

	public final Vec3 origin;
	public final String originName;
	public final Vec3 destination;
	public final String destinationName;

	public final double measurement;
	//public final double quantity;
	//public final double weight;

	public final int arrival;
	public final int departure;

	public Shipment(String shipper,
			String consignee,
			String origin,
			String destination,
			double measurement,
			String arrivalDate,
			String departureDate) throws ParseException {

		this.shipper = shipper;
		this.consignee = consignee;

		this.origin = PortUtils.getPortLocation(origin).toVec3();
		this.originName = origin;
		this.destination = PortUtils.getPortLocation(destination).toVec3();
		this.destinationName = destination;

		this.measurement = measurement;

		Calendar cal = Calendar.getInstance();

		cal.setTime(dateParser.parse(arrivalDate));
		this.arrival = (int)(cal.getTimeInMillis() / 1000L);

		cal.setTime(dateParser.parse(departureDate));
		this.departure = (int)(cal.getTimeInMillis() / 1000L);
	}


	private Shipment(String shipper,
			String consignee,
			Vec3 origin,
			Vec3 destination,
			double measurement,
			int arrivalDateSecs,
			int departureDateSecs) {
		this.shipper = shipper;
		this.consignee = consignee;
		this.origin = origin;
		this.originName = "";
		this.destination = destination;
		this.destinationName = "";
		this.measurement = measurement;
		this.arrival = arrivalDateSecs;
		this.departure = departureDateSecs;
	}

	@Override
	public double length() {
		return (shipper.isEmpty() ? 100 : 0)
				+ (consignee.isEmpty() ? 100 : 0)
				+ origin.length()
				+ destination.length();
	}

	@Override
	public Shipment subtract(final Shipment other) {
		return new Shipment(shipper.equals(other.shipper) ? shipper : "",
				consignee.equals(other.consignee) ? consignee : "",
				origin.sub(other.origin),
				destination.sub(other.destination),
				measurement - other.measurement,
				arrival - other.arrival,
				departure - other.departure);
	}

	@Override
	public Shipment add(final Shipment other) {
		return new Shipment("",
				"",
				origin.add(other.origin),
				destination.add(other.destination),
				measurement + other.measurement,
				arrival + other.arrival,
				departure + other.departure);
	}

	@Override
	public Shipment multiply(final double c) {
		return new Shipment(shipper,
				consignee,
				origin.mul(c),
				destination.mul(c),
				measurement * c,
				(int) Math.floor(arrival * c + 0.5),
				(int) Math.floor(departure * c + 0.5));
	}

	@Override
	public ShipmentLabel classLabel() {
		if (measurement < 100)
			return ShipmentLabel.NotProfitable;
		else
			return ShipmentLabel.Profitable;
	}

	public static ArrayList<Shipment> createShipmentsFromResultSet(final ResultSet set) {
		ArrayList<Shipment> shipments = new ArrayList<Shipment>();

		if (set != null) {
			try {
				while (set.next()) {
					try {
						String shipper = set.getString("sshipper");
						String consignee = set.getString("sconsignee");
						String origin = set.getString("sorigin");
						String destination = set.getString("sdestination");
						double measurement = set.getDouble("dmeasurement");
						String arrival = set.getString("heta");
						String departure = set.getString("hetd");

						Shipment s =  new Shipment(shipper,
								consignee,
								origin,
								destination,
								measurement,
								arrival,
								departure);
						shipments.add(s);
					} catch (ParseException e) {
						Log.error(e.toString());
					} catch (SQLException e) {
						Log.error(e.toString());
					}
				}
			} catch (SQLException e) {
				Log.error(e.toString());
			}
		}
		return shipments;
	}

	@Override
	public ArrayList<ShipmentAttribute> getAttributes() {
		ArrayList<ShipmentAttribute> attributes = new ArrayList<ShipmentAttribute>();
		attributes.add(new ShipmentAttribute("shipper"));
		attributes.add(new ShipmentAttribute("consignee"));
		attributes.add(new ShipmentAttribute("originName"));
		attributes.add(new ShipmentAttribute("destinationName"));
		return attributes;
	}
}
