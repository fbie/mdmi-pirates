/**
 * This file is part of dk.itu.shipments.
 *
 * dk.itu.shipments is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments;

import java.util.List;

import dk.itu.classifiers.ID3Splitter;
import dk.itu.classifiers.interfaces.IDecisionTree;

public class ID3ShipmentClassifier extends IDecisionTree<Shipment, ShipmentLabel, ShipmentAttribute> {

	public ID3ShipmentClassifier(final List<Shipment> trainingSet) {
		super(trainingSet,
				new ID3Splitter<Shipment, ShipmentLabel, ShipmentAttribute>(trainingSet.get(0).getAttributes()));
	}

}
