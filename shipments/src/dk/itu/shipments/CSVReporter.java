/**
 * This file is part of dk.itu.shipments.
 *
 * dk.itu.shipments is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments;

import java.util.ArrayList;
import java.util.HashMap;

public class CSVReporter {

	static HashMap<String, ArrayList<String>> reports =
			new HashMap<String, ArrayList<String>>();

	/**
	 * Produces a CSV entry
	 * @param type Where this entry should be added to
	 * @param pos Actual Positive
	 * @param neg Actual Negative
	 * @param false_pos False Positives
	 * @param false_neg False Negatives
	 * @param fail_pos Failed Positives
	 * @param fail_neg Failed Negatives
	 */
	public static void report(final String type, int pos, int neg, int true_pos, int true_neg, int false_pos, int false_neg, int fail_pos, int fail_neg) {
		if (!reports.keySet().contains(type))
			reports.put(type, new ArrayList<String>());
		reports.get(type).add(cat(pos, neg, true_pos, true_neg, false_pos, false_neg, fail_pos, fail_neg));
	}

	private static String cat(int pos, int neg, int true_pos, int true_neg, int false_pos, int false_neg, int fail_pos, int fail_neg) {
		return pos + "," + neg + "," + true_pos + "," + true_neg + "," + false_pos + "," + false_neg + "," + fail_pos + "," + fail_neg;
	}

	/**
	 * Prints all stored reports in CSV format
	 * to stdout.
	 */
	public static void print() {
		System.out.println("Expected Positive,Expected Negative,True Positive,True Negative,False Positive,False Negative,Fail Positive,Fail Negative");
		for (String key : reports.keySet()) {
			System.out.println("# " + key);
			for (String line : reports.get(key)) {
				System.out.println(line);
			}
		}
	}
}
