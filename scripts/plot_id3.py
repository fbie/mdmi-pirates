#! /usr/bin/python2.7

""" This script will plot a tree given in CSV format to Graphviz.
"""

import pydot
import sys
                
def depth(node_str=""):
    return node_str.count(",") 

def is_leaf(node_str=""):
    return "Leaf: " in node_str

def get_attribute(node_str=""):
    return node_str.split(",")[depth(node_str)].strip("Node: ").strip("Leaf: ")

def get_value(node_str=""):
    return node_str.split(",")[depth(node_str)-1]

def print_help():
    print "Usage: plot_id3.py [opts]"
    print "  --help           Print this help text and exit."
    print "  --filename=FILE  The file to process."
    print "  --output=FILE    Where to put the file, defaults to the original file with a different ending."
    print "  --limit=LIMIT    How many nodes to process."
    sys.exit(0)

if __name__ == "__main__":
    opts = {"filename" : "",
            "output" : (lambda : opts["filename"].strip(".csv") + "png"),
            "limit" : None}

    for arg in sys.argv[1:]:
        if arg.startswith("--"):
            opt = arg.strip("--")
            if opt == "help":
                print_help()
            else:
                key, val = opt.split("=")
                opts[key] = val

    if opts["limit"]:
        opts["limit"] = int(opts["limit"])

    progress = 1
    with open(opts["filename"], "r") as file:
        name = opts["output"]
        graph = pydot.Dot(name)

        root = pydot.Node(get_attribute(file.readline()))
        parents = [root]

        # each line is a node
        for csv_node in file.readlines():
            if opts["limit"] and opts["limit"] < progress:
                break

            # if depth does not fit, it must be above
            if depth(csv_node) < len(parents):
                parents.pop()

            node = pydot.Node(get_attribute(csv_node))
            graph.add_node(node)

            edge = pydot.Edge(parents[-1], node)
            graph.add_edge(edge)
            edge.set_label(get_value(csv_node))

            if not is_leaf(csv_node):
                parents.append(node)

            progress += 1
            if progress % 100 == 0:
                print "Processed %d nodes..." %progress

        graph.write_png(name + ".png")
