/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.common.interfaces;

/**
 * IVector is an interface that defines vector operations on
 * it's implementations.
 */
public interface IVector<VectorT extends IVector<VectorT>> {

	/**
	 * @return Distance from this sample to origin
	 */
	public double length();

	/**
	 * @param other Sample to subtract from
	 * @return A new sample representing the difference between this and other
	 */
	public VectorT subtract(final VectorT other);

	/**
	 * @param other Sample to add to
	 * @return A new sample representing the sum of this and other
	 */
	public VectorT add(final VectorT other);

	/**
	 * @param c Constant by what the sample should be scaled
	 * @return The scaled sample
	 */
	public VectorT multiply(final double c);
}
