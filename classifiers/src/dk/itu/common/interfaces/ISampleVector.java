/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.common.interfaces;

/**
 * Represents a classifiable sample vector.
 *
 * For type-safety, the type parameter LablelT is preferred,
 * since it enables ISampleVector implementations to return a properly
 * typed IClassLabel implementation. This makes it way nicer for
 * classifiers to return properly typed labels.
 */
public interface ISampleVector<SampleT extends ISampleVector<SampleT, LabelT>, LabelT extends IClassLabel>
	extends IVector<SampleT>, IClassifiable<LabelT> {

}
