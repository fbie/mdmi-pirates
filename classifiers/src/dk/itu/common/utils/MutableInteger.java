/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.common.utils;

/**
 * Idea from
 * http://stackoverflow.com/questions/81346/most-efficient-way-to-increment-a-map-value-in-java
 *
 * MutableInteger is not thread safe!
 */
public class MutableInteger {

	private int value;

	public MutableInteger(int value) {
		this.value = value;
	}

	public MutableInteger() {
		this(1);
	}

	public int get() {
		return value;
	}

	public void increment() {
		value++;
	}

	public String toString() {
		return String.valueOf(value);
	}
}
