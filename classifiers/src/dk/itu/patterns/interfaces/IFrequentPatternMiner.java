/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.patterns.interfaces;

import java.util.ArrayList;
import java.util.List;

import dk.itu.common.interfaces.IVector;

/**
 * Base class for frequent pattern mining algorithms.
 *
 * @param <SampleT> Sample type for which patterns should be identified.
 * @param <PatternT> Pattern type for given sample type.
 */
public abstract class IFrequentPatternMiner<SampleT extends IVector<SampleT>,
	PatternT extends IPattern<SampleT>> {

	public abstract ArrayList<PatternT> findFrequentPatterns(List<SampleT> samples);
}
