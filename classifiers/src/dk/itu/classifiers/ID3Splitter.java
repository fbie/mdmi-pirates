/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.classifiers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dk.itu.classifiers.interfaces.IAttribute;
import dk.itu.classifiers.interfaces.ISplitter;
import dk.itu.common.interfaces.IClassLabel;
import dk.itu.common.interfaces.ISplittable;
import dk.itu.common.utils.MutableInteger;

public class ID3Splitter<SampleT extends ISplittable<SampleT, LabelT, AttributeT>,
	LabelT extends IClassLabel, AttributeT extends IAttribute<AttributeT, SampleT, LabelT>>
	extends ISplitter<SampleT, LabelT, AttributeT> {

	public ID3Splitter(ArrayList<AttributeT> attributes) {
		super(attributes);
	}

	@Override
	public AttributeT nextSplitAttribute(List<SampleT> samples,
			List<AttributeT> attributes) {
		AttributeT bestAttribute = null;
		double maxGain = Double.NEGATIVE_INFINITY;

		for (AttributeT attribute : attributes) {
			double gain = computeGain(attribute, samples);
			if (bestAttribute == null || maxGain < gain) {
				maxGain = gain;
				bestAttribute = attribute;
			}
		}
		return bestAttribute;
	}

	/**
	 * @param samples Samples to compute the entropy of
	 * @return Entropy over samples
	 */
	private double computeEntropy(List<SampleT> samples) {
		HashMap<LabelT, MutableInteger> classes = getClasses(samples);
		double samples_d = (double)samples.size();

		double entropy = 0;
		for(MutableInteger value : classes.values()) {
			double value_p = value.get() / samples_d;
			entropy += value_p * (Math.log(value_p) / Math.log(2));
		}
		return -entropy;
	}

	/**
	 * Computes the splitting entropy for the giving splitting over samples
	 * @param splits Splitting over samples
	 * @param samples Samples that were split
	 * @return Entropy of splitting over samples
	 */
	private double computeSplitEntropy(ArrayList<ArrayList<SampleT>> splits, List<SampleT> samples) {
		double entropy = 0;
		double samples_d = (double)samples.size();
		for (ArrayList<SampleT> split : splits) {
			entropy += ((double)split.size() / samples_d) * computeEntropy(split);
		}
		return entropy;
	}

	/**
	 * Computes the information gain given by splitting on an attribute
	 * @param attribute Attribute to split on
	 * @param samples Data to split on attribute
	 * @return The information gain value
	 */
	private double computeGain(AttributeT attribute, List<SampleT> samples) {
		HashMap<Object, ArrayList<SampleT>> partitions = partition(samples, attribute);

		// need to convert from has map to list of lists
		ArrayList<ArrayList<SampleT>> splits = new ArrayList<ArrayList<SampleT>>();
		for (Object value : partitions.keySet())
			splits.add(partitions.get(value));

		return computeEntropy(samples) - computeSplitEntropy(splits, samples);
	}
}
