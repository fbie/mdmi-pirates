/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.classifiers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import dk.itu.classifiers.interfaces.IClassifier;
import dk.itu.common.interfaces.IClassLabel;
import dk.itu.common.interfaces.ISampleVector;
import dk.itu.common.utils.MutableInteger;

public class KNN<SampleT extends ISampleVector<SampleT, LabelT>, LabelT extends IClassLabel>
		extends IClassifier<SampleT, LabelT> {

	/**
	 * Reference class that stores a distance value and
	 * a reference to a sample. It is used to calculate the
	 * distance of each sample of the training set. Afterwards,
	 * all the DistanceRef objects will be sorted after distance
	 * and the k first elements of the sorted list will be
	 * assessed in majority voting.
	 */
	class DistanceRef {
		SampleT ref;
		double distance;
	}

	int k;
	List<SampleT> trainingData;
	Comparator<DistanceRef> comparator;

	/**
	 * Construct a KNN classifier for a particular k and training set.
	 * @param k How many neighbors should be assessed in majority voting?
	 * @param trainingData Data set to "train" the classifier on.
	 */
	public KNN(final int k, final List<SampleT> trainingData) {
		setK(k);
		train(trainingData);

		comparator = new Comparator<DistanceRef>() {
			public int compare(DistanceRef a, DistanceRef b) {
				double d = a.distance - b.distance;
				if (d > 0)
					return 1;
				else if (d < 0)
					return -1;
				return 0;
			}
		};
	}

	public void setK(int k) {
		this.k = k;
	}

	@Override
	public void train(final List<SampleT> samples) {
		trainingData = samples;
	}

	@Override
	public LabelT classify(final SampleT sample) {

		// Calculate all distances and store them in reference objects,
		// then sort after distance.
		ArrayList<DistanceRef> neighbors = new ArrayList<DistanceRef>();
		for (SampleT t : trainingData) {
			DistanceRef d = new DistanceRef();
			d.ref = t;
			d.distance = sample.subtract(t).length();
			neighbors.add(d);
		}
		Collections.sort(neighbors, comparator);

		// Pick the first k references.
		HashMap<LabelT, MutableInteger> amounts = new HashMap<LabelT, MutableInteger>();
		for (int i = 0; i < k; i++) {
			LabelT label = neighbors.get(i).ref.classLabel();
			MutableInteger amount = amounts.get(label);
			if (amount == null)
				amounts.put(label, new MutableInteger());
			else
				amount.increment();
		}

		// Perform majority voting on the first k references.
		// This supports n class labels
		int majority = Integer.MIN_VALUE;
		LabelT label = null;
		for (LabelT l : amounts.keySet()) {
			int amount = amounts.get(l).get();
			if (amount > majority) {
				majority = amount;
				label = l;
			}
		}

		return label;
	}
}
