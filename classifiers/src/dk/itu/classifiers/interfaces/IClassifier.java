/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.classifiers.interfaces;

import java.util.List;

import dk.itu.common.interfaces.IClassLabel;
import dk.itu.common.interfaces.IClassifiable;

/**
 * Base class for classifiers. Since the classifier needs to
 * return a proper label and also should do this as type-safe
 * as possible, SampleT needs to implement ISampleVector.
 */
public abstract class IClassifier<SampleT extends IClassifiable<LabelT>, LabelT extends IClassLabel> {

	/**
	 * Train the classifier on the given dataset.
	 *
	 * @param samples The dataset to train on
	 */
	public abstract void train(final List<SampleT> samples);

	/**
	 * Classify a given sample based on previous training.
	 *
	 * @param sample The sample to classify
	 * @return The class, sample belongs to. null, if an error occurs.
	 */
	public abstract LabelT classify(final SampleT sample);

}
