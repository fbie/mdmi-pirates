/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.classifiers.interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import dk.itu.common.interfaces.IClassLabel;
import dk.itu.common.interfaces.ISplittable;
import dk.itu.common.utils.MutableInteger;

public abstract class IDecisionTree <SampleT extends ISplittable<SampleT, LabelT, AttributeT>,
	LabelT extends IClassLabel, AttributeT extends IAttribute<AttributeT, SampleT, LabelT>>
	extends IClassifier<SampleT, LabelT> {

	/**
	 * Represents a node in the decision tree
	 */
	protected class Node {

		public final AttributeT splittingCriterion;
		public final HashMap<Object, Node> children;
		public final LabelT classLabel;

		public Node(final LabelT classLabel) {
			this(null, classLabel);
		}

		public Node(final AttributeT criterion) {
			this(criterion, null);
		}

		private Node(final AttributeT criterion, final LabelT classLabel) {
			splittingCriterion = criterion;
			this.classLabel = classLabel;
			children = new HashMap<Object, Node>(0);
		}

		public boolean isLeaf() {
			return children.isEmpty();
		}

		@Override
		public String toString() {
			if (isLeaf()) {
                return "Leaf: " + classLabel;
	        } else {
	            return "Node: " + splittingCriterion;
	        }
		}

		private String toStringRecursive(final String prefix, final int depth) {
			String r = "";

			// add distance to left margin
			for (int i = 0; i < depth; i++)
				r += ",";

			r += prefix + "," + toString();

			// append children, increase distance
			for (Object branch : children.keySet()) {
				Node child = children.get(branch);
				r += "\n" + child.toStringRecursive(branch.toString(), depth + 1);
			}

			return r;
		}
	}

	protected ISplitter<SampleT, LabelT, AttributeT> splitter;
	protected Node root;

	public IDecisionTree (List<SampleT> trainingSet, ISplitter<SampleT,LabelT, AttributeT> splitter) {
		setSplitter(splitter);
		train(trainingSet);
	}

	public void setSplitter(ISplitter<SampleT, LabelT, AttributeT> splitter) {
		this.splitter = splitter;
	}

	private int countClasses(final List<SampleT> samples) {
		HashSet<LabelT> seenLabels = new HashSet<LabelT>();
		for (SampleT sample : samples) {
			seenLabels.add(sample.classLabel());
		}
		return seenLabels.size();
	}

	private LabelT majorityVote(final List<SampleT> samples) {
		final HashMap<LabelT, MutableInteger> classes = splitter.getClasses(samples);
		List<LabelT> classList = new ArrayList<LabelT>(classes.keySet());

		// sort after entries in hash map
		Collections.sort(classList, new Comparator<LabelT>(){
			@Override
			public int compare(LabelT arg0, LabelT arg1) {
				return classes.get(arg1).get() - classes.get(arg0).get();
			}
		});

		return classList.get(0);
	}

	/**
	 * Build a decision tree using splitter
	 * @param samples
	 * @return A node in the decision tree
	 */
	protected Node buildTree(final List<SampleT> samples, final List<AttributeT> attributes) {

		// no attributes left, perform majority voting
		if (attributes.isEmpty()) {
			return new Node(majorityVote(samples));
		} else if (countClasses(samples) == 1) {
			return new Node(samples.get(0).classLabel());
		} else {

			// compute a new node and its children, recursively
			AttributeT splittingCriterion = splitter.nextSplitAttribute(samples, attributes);

			List<AttributeT> newAttributes = new ArrayList<AttributeT>(attributes);
			newAttributes.remove(splittingCriterion);

			// add further children recursively
			final HashMap<Object, ArrayList<SampleT>> partitions = splitter.partition(samples, splittingCriterion);

			Node node = new Node(splittingCriterion);
			for (Object value : partitions.keySet()) {
				Node child;
				// perform majority voting on the current set if no outcome for attribute
				if (partitions.get(value).isEmpty()) {
					child = new Node(majorityVote(samples));
				// otherwise, call recursively
				} else {
					child = buildTree(partitions.get(value), newAttributes);
				}
				node.children.put(value, child);
			}

			return node;
		}
	}

	@Override
	public void train(final List<SampleT> samples) {
		root = buildTree(samples, splitter.getAttributes());
		root = prune(root);
	}

	private Node prune(Node node) {
		if (node.isLeaf())
			return node;

		// first, prune children
		for (Object branch : node.children.keySet()) {
			Node child = prune(node.children.get(branch));
			node.children.put(branch, child);
		}

		// then decide if this node should be pruned
		ArrayList<LabelT> classes = new ArrayList<LabelT>();
		for (Node child: node.children.values()) {

			// if there is at least one child that is not a leaf, we cannot prune
			if (!child.isLeaf())
				return node;

			// otherwise, put the label into a hash map
			if (!classes.contains(child.classLabel)) {
				classes.add(child.classLabel);

				// shortcut: if there are more than two classes, we cannot prune
				if (classes.size() > 1)
					return node;
			}
		}

		// prune if all children are redundant
		if (classes.size() == 1)
			return new Node(classes.get(0));
		return node;
	}

	@Override
	public LabelT classify(final SampleT sample) {
		return traverse(sample, root);
	}

	private LabelT traverse(final SampleT sample, final Node node) {
		if (node.isLeaf())
			return node.classLabel;

		AttributeT criterion = node.splittingCriterion;
		Node child;
		if (criterion.isContinuousValue()) {
			double value = criterion.getNumericalValue(sample);
			child = node.children.get(value > criterion.getSplitValue());
		} else {
			Object value = criterion.getValue(sample);
			child = node.children.get(value);
		}

		// FIXME this is put here for debugging purposes until we find a fix for this
		if (child == null)
			return null;
		return traverse(sample, child);
	}

	/**
	 * @return CSV representation of the decision tree if tree is not empty.
	 */
	@Override
	public String toString() {
		if (root != null)
			return root.toStringRecursive("root", 0);
		return super.toString();
	}
}
