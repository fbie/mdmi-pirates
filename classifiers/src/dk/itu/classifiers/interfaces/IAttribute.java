/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.classifiers.interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import dk.itu.common.interfaces.IClassLabel;
import dk.itu.common.interfaces.ISplittable;

public abstract class IAttribute<AttributeT extends IAttribute<AttributeT, SplittableT, LabelT>,
	SplittableT extends ISplittable<SplittableT, LabelT, AttributeT>, LabelT extends IClassLabel> {

	public final String attributeName;
	public final boolean continuouslyValued;
	public double splitValue;

	public IAttribute(final String attribute, final boolean contValued) {
		attributeName = attribute;
		continuouslyValued = contValued;
		splitValue = 0;
	}

	public IAttribute(String attribute) {
		this(attribute, false);
	}

	/**
	 * Return the value of the attribute for a given splittable
	 * @param splittable Sample for which the value is requested
	 * @return The value
	 */
	public final Object getValue(final SplittableT splittable) {
		try {
			return splittable.getClass().getDeclaredField(attributeName).get(splittable);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return null;
	}

	public final double getNumericalValue(final SplittableT splittable) {
		try{
			return splittable.getClass().getDeclaredField(attributeName).getDouble(splittable);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return 0;
	}

	/**
	 * @return True of continuously valued, false otherwise
	 */
	public boolean isContinuousValue() {
		return continuouslyValued;
	}

	/**
	 * Computes the split value of continuously valued attributes.
	 * Should be overridden, as splitting my median is representing
	 * zero domain knowledge.
	 * @param samples Samples for which the split value should be found
	 * @return The split value for samples
	 */
	public double getSplitValue(final List<SplittableT> samples) {
		if (!isContinuousValue())
			return 0;

		splitValue = computeMedian(samples);
		return splitValue;
	}

	public double getSplitValue() {
		return splitValue;
	}

	/**
	 * Compute the mean of this attribute if it is continuously valued
	 * @param samples To calculate the mean over
	 * @return Mean value of samples
	 */
	private final double computeMedian(final List<SplittableT> samples) {
		ArrayList<SplittableT> sorted = new ArrayList<SplittableT>(samples);
		Collections.sort(sorted, new Comparator<SplittableT>() {
			@Override
			public int compare(SplittableT a, SplittableT b) {
				double a_val = getNumericalValue(a);
				double b_val = getNumericalValue(b);
				if (a_val > b_val)
					return 1;
				else if (a_val < b_val)
					return -1;
				else
					return 0;
			}
		});

		return getNumericalValue(sorted.get(sorted.size() / 2));
	}

	public String toString() {
		return attributeName + (isContinuousValue() ? "=" + splitValue : "");
	}
}
