/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.classifiers.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dk.itu.common.interfaces.IClassLabel;
import dk.itu.common.interfaces.ISplittable;
import dk.itu.common.utils.MutableInteger;

public abstract class ISplitter<SplittableT extends ISplittable<SplittableT, LabelT, AttributeT>,
	LabelT extends IClassLabel, AttributeT extends IAttribute<AttributeT, SplittableT, LabelT>> {

	final protected List<AttributeT> attributes;

	public ISplitter(final List<AttributeT> attributes) {
		this.attributes = attributes;
	}

	/**
	 * @param samples Remaining samples
	 * @param attributes Remaining attributes
	 * @return The attribute to split on next
	 */
	public abstract AttributeT nextSplitAttribute(final List<SplittableT> samples, final List<AttributeT> attributes);

	public final ArrayList<AttributeT> getAttributes() {
		return new ArrayList<AttributeT>(attributes);
	}

	/**
	 * Count the number of class labels distributed over the given samples
	 * @param samples List of splittables
	 * @return A map class label -> mutable integer
	 */
	public final HashMap<LabelT, MutableInteger> getClasses(final List<SplittableT> samples) {
		HashMap<LabelT, MutableInteger> classes = new HashMap<LabelT, MutableInteger>();

		for (SplittableT sample : samples) {
			if (!classes.containsKey(sample.classLabel()))
				classes.put(sample.classLabel(), new MutableInteger(0));
			classes.get(sample.classLabel()).increment();
		}
		return classes;
	}

	/**
	 * @param samples Samples to be partitioned
	 * @param attribute Attribute to be partitioned on
	 * @return
	 */
	public final HashMap<Object, ArrayList<SplittableT>> partition(final List<SplittableT> samples,
			AttributeT attribute) {

		HashMap<Object, ArrayList<SplittableT>> partitions =
				new HashMap<Object, ArrayList<SplittableT>>(0);

		// continuously valued attributes are only split binary
		if (attribute.isContinuousValue()) {
			double splitValue = attribute.getSplitValue(samples);

			for (SplittableT sample : samples) {
				boolean branch = attribute.getNumericalValue(sample) > splitValue;
				if (!partitions.containsKey(branch))
					partitions.put(branch,  new ArrayList<SplittableT>());
				partitions.get(branch).add(sample);
			}

		// discretely valued attributes are split multi-way
		} else {
			for (SplittableT sample : samples) {
				Object value = attribute.getValue(sample);
				if (!partitions.keySet().contains(value))
					partitions.put(value, new ArrayList<SplittableT>());
				partitions.get(value).add(sample);
			}
		}

		return partitions;
	}

}
