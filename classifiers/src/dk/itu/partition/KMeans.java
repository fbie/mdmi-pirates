/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.partition;

import java.util.ArrayList;
import java.util.List;

import dk.itu.common.interfaces.IVector;
import dk.itu.partition.interfaces.IPartitioner;

/**
 * Partitions samples using the k-means algorithm.
 */
public class KMeans<SampleT extends IVector<SampleT>> extends IPartitioner<SampleT> {

	int k;

	public KMeans(int k) {
		setK(k);
	}

	public void setK(int k) {
		this.k = k;
	}

	@Override
	public ArrayList<ArrayList<SampleT>> partition(final List<SampleT> samples) {
		if (k > samples.size())
			return null;

		// Choose k centroids arbitrarily (i.e. the first k)
		ArrayList<SampleT> centroids = new ArrayList<SampleT>(samples.subList(0, k));

		// Re-assign partitions until the error doesn't improve anymore
		double error = Double.MAX_VALUE;
		ArrayList<ArrayList<SampleT>> partitions = new ArrayList<ArrayList<SampleT>>(k);

		while (true) {
			ArrayList<ArrayList<SampleT>> currentPartitions = new ArrayList<ArrayList<SampleT>>(k);
			for (int i = 0; i < k; i++) {
				currentPartitions.add(new ArrayList<SampleT>());
			}

			// Assign samples to partitions w.r.t. centroid
			// (i.e. minimize distance to centroid)
			for (SampleT s : samples) {
				int closestCentroidIdx = 0;
				double closestDistance = Double.MAX_VALUE;

				for (int i = 0; i < k; i++) {
					double distance = s.subtract(centroids.get(i)).length();
					if (distance < closestDistance) {
						closestDistance = distance;
						closestCentroidIdx = i;
					}
				}
				currentPartitions.get(closestCentroidIdx).add(s);
			}

			// Recalculate centroids
			for (int i = 0; i < k; i++) {
				centroids.set(i, centroid(currentPartitions.get(i)));
			}

			// Check if the error improved and stop if not
			double currentError = squaredError(centroids, currentPartitions);
			if (currentError >= error)
				break;

			error = currentError;
			partitions = currentPartitions;
		}
		return partitions;
	}

	/**
	 * Computes the squared error for partitions and their respective
	 * centroids. Both lists must be equal in length.
	 *
	 * @param centroids Centroids according to partitions
	 * @param partitions Partitions according to centroids
	 * @return The squared error for centroids given partition
	 */
	private double squaredError(final ArrayList<SampleT> centroids, final ArrayList<ArrayList<SampleT>> partitions) {
		double error = 0;
		for (int i = 0; i < k; i++) {
			List<SampleT> partition = partitions.get(i);
			SampleT centroid = centroids.get(i);
			for (SampleT s : partition) {
				double distance = s.subtract(centroid).length();
				error += distance * distance;
			}
		}
		return error;
	}

	/**
	 * Computes the centroid for a given partition of samples
	 *
	 * @param partition A partition of samples for which the centroid should be computed
	 * @return The centroid for partition
	 */
	private SampleT centroid(final ArrayList<SampleT> partition) {
		SampleT average = partition.get(0);
		for (SampleT s : partition.subList(1, partition.size()))
			average = average.add(s);
		return average.multiply(1.0 / partition.size());
	}
}
