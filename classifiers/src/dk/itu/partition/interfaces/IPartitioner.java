/**
 * This file is part of dk.itu.classifiers.
 *
 * dk.itu.classifiers is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.classifiers is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.classifiers. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.partition.interfaces;

import java.util.ArrayList;
import java.util.List;

import dk.itu.common.interfaces.IVector;

/**
 * Base class for algorithms that partition a list
 * of samples. No labeling is required here, therefore
 * SampleT is only required to implement IVector.
 */
public abstract class IPartitioner<SampleT extends IVector<SampleT>> {

	/**
	 * Partition a set of samples into a number of partitions
	 *
	 * @param samples Data set to partition
	 * @return A set of data partitions of samples
	 */
	public abstract ArrayList<ArrayList<SampleT>> partition(final List<SampleT> samples);

}
