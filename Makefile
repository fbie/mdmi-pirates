CLASSPATH = "shipments/src/:classifiers/src/:utils/src/:utils/mysql-connector-java-5.1.24/mysql-connector-java-5.1.24-bin.jar"

BIN = bin

all: classifier

classifier:
	javac -cp $(CLASSPATH) -verbose -d $(BIN) shipments/src/dk/itu/shipments/Classify.java

clean:
	rm -rf $(BIN)
	mkdir $(BIN)
