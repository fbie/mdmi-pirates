/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import dk.itu.shipments.utils.coords.GpsCoordinates;

public class PortUtils {

	/**
	 * Translates a port code into the corresponding human readable name.
	 * @param portCode to be translated.
	 * @return The corresponding name; if the query fails, the port code is returned.
	 */
	public static String getPortName(String portCode) {
		String name = portCode;
		String query = "* from PortCodes where PortCode = \"" + portCode + "\"";

		try {
			ResultSet result = Backend.select(query);
			if (result.next())
				name = result.getString("Name_Clean");
			result.close();
		} catch (SQLException e) {
			Log.error(e.toString());
		}

		return name;
	}

	// cache for already looked up port coordinates
	private static HashMap<String, GpsCoordinates> portCoordinates =
			new HashMap<String, GpsCoordinates>();

	public static GpsCoordinates getPortLocation(String portCode) {

		// return cached coordinates
		if (portCoordinates.containsKey(portCode))
			return portCoordinates.get(portCode);

		// perform query
		String query = "* from PortCodes where PortCode = \"" + portCode + "\"";

		try {
			ResultSet result = Backend.select(query);
			if (result.next()) {
				double lat = result.getDouble("latitude");
				double lon = result.getDouble("longitude");

				// generate coordinates object and cache it
				GpsCoordinates coords = new GpsCoordinates(lat, lon);
				portCoordinates.put(portCode, coords);
				return coords;
			}
			result.close();
		} catch (SQLException e) {
			Log.error(e.toString());
		}

		// FIXME return something more useful than this
		return new GpsCoordinates(0, 0);
	}
}
