/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils.upload;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

import dk.itu.shipments.utils.Backend;
import dk.itu.shipments.utils.Log;

public abstract class BaseUploader {

	static int MAX_LINES = -1;
	static boolean DRY = false;

	public abstract String lineToStatement(String line);

	public String clean(String pre, String token, String post) {
		return token.isEmpty() ? "NULL" : pre + " \"" + token + "\" " + post;
	}

	public String clean(String token) {
		return clean("", token, "");
	}

	public String cast(String token, String type) {
		return clean("CAST(", token, "AS " + type + ")");
	}

	public String convertDatetime(String token) {
		return clean("CONVERT(", token, ", Datetime)");
	}

	public void upload(String [] args) {
		if (args.length < 1 || !args[0].endsWith(".csv")) {
			Log.error("No path to .csv file given, aborting.");
			System.exit(1);
		}

		String filename = args[0];
		try {
			FileInputStream file = new FileInputStream(filename);
			Scanner scanner = new Scanner(file);

			if (!DRY)
				Backend.connect();

			// skip first line, it contains only variable types
			if (scanner.hasNextLine())
				scanner.nextLine();

			int line = 1;
			int errors = 0;
			while (scanner.hasNextLine() && (MAX_LINES < 0 || line <= MAX_LINES)) {
				try {
					String statement = lineToStatement(scanner.nextLine());

					if (!DRY)
						Backend.insertInto(statement);

				} catch (Exception e) {
					Log.error(e.toString()
							+ " - Line: "
							+ line
							+ " - Errors: "
							+ ++errors);
				} finally {
					line++;
				}
			}

			if (!DRY)
				Backend.disconnect();
			scanner.close();

			Log.debug(0, "Uploaded "
					+ (line - 1) // we started at line 1
					+ " entries, encountered "
					+ errors
					+ " errors.");

		} catch (FileNotFoundException e) {
			Log.error(e.getLocalizedMessage());
			System.exit(1);
		}
	}
}
