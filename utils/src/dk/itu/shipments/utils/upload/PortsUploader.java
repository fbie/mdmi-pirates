/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils.upload;

import dk.itu.shipments.utils.Log;

public class PortsUploader extends BaseUploader {

	String table;

	public PortsUploader(String table) {
		this.table = table;
	}

	public String parseCoordinates(String token) {
		String[] coords = token.split(" ");

		if (coords.length != 2)
			return "NULL, NULL";

		String lat = coords[0].endsWith("N") ? "" : "-";
		lat += coords[0].substring(0, 2) + "." + coords[0].substring(2, 4);

		String lng = coords[1].endsWith("E") ? "" : "-";
		if (coords[1].length() == 5) {
			lng += coords[1].substring(0, 2) + "." + coords[1].substring(2, 4);
		} else {
			lng += coords[1].substring(0, 3) + "." + coords[1].substring(3, 5);
		}

		return cast(lat, "DECIMAL(10,6)") + ", " + cast(lng, "DECIMAL(10,6)");
	}

	@Override
	public String lineToStatement(String line) {
		String[] tokens = line.replaceAll("\"", "").split(",", -1);

		if (tokens.length < 11)
			System.out.println(line.replaceAll("\"", ""));

		String statement = table + " VALUES("
				+ clean(tokens[0]) + ", "
				+ clean(tokens[1]) + ", "
				+ clean(tokens[2]) + ", "
				+ clean(tokens[3]) + ", "
				+ clean(tokens[4]) + ", "
				+ clean(tokens[5]) + ", "
				+ clean(tokens[6]) + ", "
				+ clean(tokens[7]) + ", "
				+ cast(tokens[8], "UNSIGNED") + ", "
				+ clean(tokens[9]) + ", "
				+ parseCoordinates(tokens[10]) + ", "
				+ clean(tokens[1] + tokens[2]) + ")";

		Log.debug(3, "Created statement: " + statement);
		return statement;
	}

	public static void main(String[] args) {
		PortsUploader uploader = new PortsUploader("PortCodes");
		uploader.upload(args);
	}

}
