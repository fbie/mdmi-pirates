/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils.upload;

import dk.itu.shipments.utils.Log;

/**
 * Only supposed to be used for uploading to database initially!
 */
public class ShipmentsUploader extends BaseUploader {

	String table;

	public ShipmentsUploader(String table) {
		this.table = table;
	}

	@Override
	public String lineToStatement(String line) {
		String[] tokens = line.split(",",-1);

		String statement = table + " VALUES("
				+ convertDatetime(tokens[0]) +  ","
				+ clean(tokens[1]) + ","
				+ clean(tokens[2]) + ","
				+ clean(tokens[3]) + ","
				+ clean(tokens[4]) + ","
				+ clean(tokens[5]) + ","
				+ clean(tokens[6]) + ","
				+ cast(tokens[7], "UNSIGNED") + ","
				+ cast(tokens[8], "DECIMAL") + ","
				+ cast(tokens[9], "DECIMAL") + ","
				+ convertDatetime(tokens[10]) +  ","
				+ convertDatetime(tokens[11]) +  ","
				+ convertDatetime(tokens[12]) +  ","
				+ convertDatetime(tokens[13]) +  ","
				+ convertDatetime(tokens[14]) +  ","
				+ convertDatetime(tokens[15]) +  ")";

		Log.debug(3, "Creating statement: " + statement);
		return statement;
	}

	public static void main(String[] args) {
		ShipmentsUploader uploader = new ShipmentsUploader("PurchaseOrders");
		uploader.upload(args);
	}

}
