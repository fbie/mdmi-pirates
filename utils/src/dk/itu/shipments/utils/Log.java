/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {

	static private int level = 0;
	static private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static void setLevel(int level) {
		Log.level = level;
	}

	public static void debug(int level, String message) {
		if (level <= Log.level)
			System.err.println(getDateTime()
					+ " [DEBUG " + level + "] "
					+ getFileAndLineNumber()
					+  ": "
					+ message);
	}

	public static void error(String message) {
		System.err.println(getDateTime()
				+ " [ERROR] "
				+ getFileAndLineNumber()
				+": "
				+ message);
	}

	private static String getFileAndLineNumber() {
		// TODO: This is expensive; pre-processing is faster!
//		StackTraceElement[] traces = Thread.currentThread().getStackTrace();
//		StackTraceElement caller = traces[3];
//		return caller.getFileName() + ":" + caller.getLineNumber();
		return "";
	}

	private static String getDateTime() {
		return "[" + dateFormatter.format(Calendar.getInstance().getTime()) + "]";
	}
}
