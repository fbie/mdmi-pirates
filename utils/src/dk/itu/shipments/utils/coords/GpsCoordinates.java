/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils.coords;

public class GpsCoordinates {

	private final double latitude_rad;
	private final double longitude_rad;

	/**
	 * Represents GPS coordinates. Coordinates are
	 * internally stored as radians.
	 *
	 * Computations and conversions after http://www.geomidpoint.com/calculation.html
	 *
	 * @param latitude as decimal
	 * @param longitude as decimal
	 */
	public GpsCoordinates(double latitude, double longitude) {
		this.latitude_rad = latitude * (Math.PI / 180);
		this.longitude_rad = longitude * (Math.PI / 180);
	}

	/**
	 * Compute GpsCoordinates from a 3D vector.
	 * @param vec Vector representation of these coordinates.
	 */
	public GpsCoordinates(Vec3 vec) {
		this.longitude_rad = Math.atan2(vec.y, vec.x);
		this.latitude_rad = Math.atan2(vec.z, Math.sqrt(vec.x * vec.x + vec.y * vec.y));
	}

	/**
	 * Compute a 3D vector from these coordinates.
	 * @return Vector representation of these coordinates.
	 */
	public Vec3 toVec3() {
		return new Vec3(Math.cos(latitude_rad) * Math.cos(longitude_rad),
				Math.cos(latitude_rad) * Math.sin(longitude_rad),
				Math.sin(latitude_rad));
	}

	public double getLatitude() {
		return latitude_rad * (180 / Math.PI);
	}

	public double getLongitude() {
		return longitude_rad * (180 / Math.PI);
	}
}
