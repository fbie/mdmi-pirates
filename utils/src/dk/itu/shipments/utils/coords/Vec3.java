/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils.coords;

/**
 * Represents a vector in 3-space.
 */
public class Vec3 {

	public final double x;
	public final double y;
	public final double z;

	public Vec3(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public double length() {
		return Math.sqrt(x * x + y * y + z * z);
	}

	public Vec3 add(Vec3 other) {
		return new Vec3(x + other.x,
				y + other.y,
				z + other.z);
	}

	public Vec3 sub(Vec3 other) {
		return new Vec3(x - other.x,
				y - other.y,
				z - other.z);
	}

	public Vec3 mul(double c) {
		return new Vec3(x * c,
				y * c,
				z * c);
	}

	public Vec3 div(double c) {
		return mul(1 / c);
	}

	/**
	 * @return This vector with unit length.
	 */
	public Vec3 toUnit() {
		return mul(1 / length());
	}

	public String toString() {
		return "Vec(" + x + ", " + y + ", " + z + ")";
	}
}
