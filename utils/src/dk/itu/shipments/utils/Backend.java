/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Wraps a JDBC connection for our purposes.
 *
 */
public class Backend {

	private static Connection connection;

	// TODO: Don't hardcode, use config file or args
	private static final String url = "jdbc:mysql://mysql.itu.dk/mdmi_shipments";
	private static final String user = "pirates";
	private static final String password = "p0llyW4nt5Cr4ckers";

	public static boolean isConnected() {
		boolean connected = false;
		try {
			connected = connection != null && !connection.isClosed();
		} catch (SQLException e) {
			Log.error(e.getLocalizedMessage());
		}
		return connected;
	}

	public static void connect() {
		if (!isConnected()) {
			try {
				connection = DriverManager.getConnection(url, user, password);
			} catch (SQLException e) {
				Log.error(e.getLocalizedMessage());
			}
			Log.debug(7, "Connected to database");
		} else {
			Log.error("Already connected to database!");
		}
	}

	public static void disconnect() {
		if (isConnected()) {
			try {
				connection.close();
			} catch (SQLException e) {
				Log.error(e.getLocalizedMessage());
			}
			Log.debug(7, "Disconnected from database");
		} else {
			Log.error("Not connected to database!");
		}
	}

	public static void reconnect() {
		disconnect();
		connect();
	}

	public static ResultSet select(String query) throws SQLException {
		if (isConnected()) {
			Statement select = connection.createStatement();
			select.execute("select " + query);
			//select.closeOnCompletion(); // FIXME since the test is running on jackdalton.itu.dk, we need to comply to JRE6
			return select.getResultSet();
		} else {
			Log.error("Not connected to database!");
		}
		return null;
	}

	public static ResultSet selectNoThrow(String query) {
		try {
			return select(query);
		} catch (SQLException e) {
			Log.error(e.getLocalizedMessage());
		}
		return null;
	}

	public static void insertInto(String query) throws SQLException {
		if (isConnected()) {
			Statement insert = connection.createStatement();
			insert.execute("INSERT INTO " + query);
			insert.close();
		} else {
			Log.error("Not connected to database!");
		}
	}

	public static void insertIntoNoThrow(String query) {
		try {
			insertInto(query);
		} catch (SQLException e) {
			Log.error(e.getLocalizedMessage());
		}
	}
}
