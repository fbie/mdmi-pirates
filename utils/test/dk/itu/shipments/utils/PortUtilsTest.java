/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dk.itu.shipments.utils.coords.GpsCoordinates;

public class PortUtilsTest {

	static double EPSILON = 0.00001;

	@Before
	public void setUp() throws Exception {
		Backend.connect();
	}

	@After
	public void tearDown() throws Exception {
		Backend.disconnect();
	}

	@Test
	public void testGetPortName() {
		assertEquals("Andorra la Vella", PortUtils.getPortName("ADALV"));
		assertEquals("Xiang Shui", PortUtils.getPortName("CNXHU"));
		assertEquals("FOOBAR", PortUtils.getPortName("FOOBAR"));
	}

	@Test
	public void testGetPortLocation() {
		GpsCoordinates coords = PortUtils.getPortLocation("ADALV");
		assertEquals(42.3, coords.getLatitude(), EPSILON);
		assertEquals(1.31, coords.getLongitude(), EPSILON);
	}

}
