/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils.upload;

import static org.junit.Assert.*;
import org.junit.*;

public class ShipmentsUploaderTest {

	ShipmentsUploader uploader;

	@Before
	public void setUp() {
		uploader = new ShipmentsUploader("NotExisting");
	}

	@After
	public void tearDown() {

	}

	@Test
	public void testLineToStatementValid() {
		String line = "2013-01-01,"
				+ "AB,"
				+ "BC,"
				+ "CD,"
				+ "DE,"
				+ "EF,"
				+ "FG,"
				+ "42,"
				+ "3.14,"
				+ "0.2,"
				+ "2013-01-01,"
				+ "2013-01-01,"
				+ "2013-01-01,"
				+ "2013-01-01,"
				+ "2013-01-01,"
				+ "2013-01-01";

		String statement = "NotExisting VALUES("
				+ "CONVERT( \"2013-01-01\" , Datetime),"
				+ " \"AB\" ,"
				+ " \"BC\" ,"
				+ " \"CD\" ,"
				+ " \"DE\" ,"
				+ " \"EF\" ,"
				+ " \"FG\" ,"
				+ "CAST( \"42\" AS UNSIGNED),"
				+ "CAST( \"3.14\" AS DECIMAL),"
				+ "CAST( \"0.2\" AS DECIMAL),"
				+ "CONVERT( \"2013-01-01\" , Datetime),"
				+ "CONVERT( \"2013-01-01\" , Datetime),"
				+ "CONVERT( \"2013-01-01\" , Datetime),"
				+ "CONVERT( \"2013-01-01\" , Datetime),"
				+ "CONVERT( \"2013-01-01\" , Datetime),"
				+ "CONVERT( \"2013-01-01\" , Datetime))";

		System.err.println(statement);
		System.err.println(uploader.lineToStatement(line));

		assertTrue(uploader.lineToStatement(line).equals(statement));
	}

	@Test
	public void testLineToStatementInvalid() {
		String line = ",,,,,,,,,,,,,,,";

		String statement = "NotExisting VALUES("
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL,"
				+ "NULL)";

		System.err.println(statement);
		System.err.println(uploader.lineToStatement(line));

		assertTrue(uploader.lineToStatement(line).equals(statement));
	}
}
