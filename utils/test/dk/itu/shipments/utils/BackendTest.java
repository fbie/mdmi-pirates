/**
 * This file is part of dk.itu.shipments.utils.
 *
 * dk.itu.shipments.utils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * dk.itu.shipments.utils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with dk.itu.shipments.utils. If not, see <http://www.gnu.org/licenses/>.
 */

package dk.itu.shipments.utils;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BackendTest {

	@Before
	public void setUp() {
		Backend.connect();
	}

	@After
	public void tearDown() {
		Backend.disconnect();
	}

	@Test
	public void testConnectivity() {
		assertTrue(Backend.isConnected());
		Backend.disconnect();
		assertFalse(Backend.isConnected());
	}

	@Test
	public void testSelect() throws SQLException {
		ResultSet results = Backend.select("test_entry from junit_test_table");
		assertNotNull(results);
		results.close();
	}

	@Test
	public void testInsert() throws SQLException {
		// FIXME Since entries in the DB are ordered by values, this test fails
		int test = (int) Math.floor(Math.random() * 100);
		Backend.insertInto("junit_test_table VALUES(CAST(\"" + test + "\" AS UNSIGNED))");
		ResultSet results = Backend.select("test_entry from junit_test_table");
		results.last();
		assertEquals(test, results.getInt(1));
		results.close();
	}

}
